/// @description 
if(hp > 0)
{
	switch(state)
	{
		case S_MOVE:
			scr_set_animation(s_knight_walk_strip);
			image_xscale = x < obj_player.x ? 1 : -1;
			mp_linear_step_object(obj_player.x, obj_player.y, spd, noone);
			
			if(distance_to_object(obj_player) < 25)
				state = S_ATTACK_1;
		break;
		
		case S_ATTACK_1:
			scr_set_animation(s_knight_attack_strip);
			
			if(image_index == 4)
			{
				var hitbox = instance_create_layer(x, y, "Instances", obj_hitbox);
				hitbox.sprite_index = s_knight_attack_damage;
				hitbox.image_xscale = image_xscale;
				hitbox.damage = dmg;
			}
		break;
		
		case S_HIT:
			scr_set_animation(s_knight_hitstun);
			x += image_xscale * -1;
		break;
		
		case S_IDLE:
			scr_set_animation(s_knight_idle_strip);
		break;
	}
}
else if(state != S_DEAD)
{
	scr_set_animation(s_knight_die_strip);
	state = S_DEAD;
}