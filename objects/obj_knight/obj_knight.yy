{
    "id": "8a43249e-6183-4049-a964-15b2820abca7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_knight",
    "eventList": [
        {
            "id": "5db07a34-20eb-48b5-85c7-6d5b135fe650",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a43249e-6183-4049-a964-15b2820abca7"
        },
        {
            "id": "55514ec5-9233-4fe5-a6fc-4f1328298f61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a43249e-6183-4049-a964-15b2820abca7"
        },
        {
            "id": "2d20ae49-85ab-4fee-8204-44f6221aad65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8a43249e-6183-4049-a964-15b2820abca7"
        },
        {
            "id": "3d304239-f868-4464-a7cc-c058787869b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8a43249e-6183-4049-a964-15b2820abca7"
        },
        {
            "id": "d8b5b0b9-b2cf-45ab-b221-41cd10381717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8a43249e-6183-4049-a964-15b2820abca7"
        }
    ],
    "maskSpriteId": "f5cb645d-5b73-4c6c-8361-3b383b9f5f3f",
    "overriddenProperties": null,
    "parentObjectId": "824a5592-2b2a-4e6f-86fb-bc7f549b9e3e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
    "visible": true
}