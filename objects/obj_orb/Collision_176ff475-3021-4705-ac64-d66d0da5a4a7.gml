/// @description
with(other)
{
	if(state != S_DEAD)
	{
		xp++;
		total_xp++;
		if(xp == max_xp)
		{
			lvl++;
			max_hp += 3;
			hp = max_hp;
			max_xp += 25;
			xp = 0;
		}
	}
}

instance_destroy();