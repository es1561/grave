{
    "id": "50ed9cd1-1d9a-4b7e-80dc-6e4a29515948",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_orb",
    "eventList": [
        {
            "id": "d043842c-9cc0-42d3-a697-f1f39a2f7b04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "50ed9cd1-1d9a-4b7e-80dc-6e4a29515948"
        },
        {
            "id": "4342794f-3bd7-4605-a16d-d3cd629a2f6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50ed9cd1-1d9a-4b7e-80dc-6e4a29515948"
        },
        {
            "id": "4c510453-908f-465b-8d8b-6bfec4bf9327",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "50ed9cd1-1d9a-4b7e-80dc-6e4a29515948"
        },
        {
            "id": "176ff475-3021-4705-ac64-d66d0da5a4a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1ec7baa8-2f31-4219-abd3-6567429aa256",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "50ed9cd1-1d9a-4b7e-80dc-6e4a29515948"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2ecda4c-cba1-4559-b7ae-5bec760306ba",
    "visible": true
}