/// @description 

if(hp > 0)
{
	switch(state)
	{
		case S_MOVE:
			var dir = keyboard_check(vk_right) - keyboard_check(vk_left);
			var hspd = dir * 2;

			if(dir != 0)
			{
				sprite_index = s_skeleton_run_strip;
				image_xscale = dir;
			}
			else
				sprite_index = s_skeleton_idle_strip;

			if(place_meeting(x + hspd, y, obj_frence))
			{
				while(!place_meeting(x + dir, y, obj_frence))
					x += dir;
	
				hspd = 0;
			}
			x += hspd;

			if(!f_roll && keyboard_check_pressed(ord("C")))
				state = S_ROLL;
			else if(keyboard_check_pressed(vk_space))
				state = S_ATTACK_1;
		break;
	
		case S_ROLL:
			var hspd = image_xscale * 3;
			
			f_immune = true;
			f_roll = true;
			scr_set_animation(s_skeleton_roll_strip);
			alarm[1] = room_speed div 3;
			alarm[2] = room_speed div 5;
			
			if(place_meeting(x + hspd, y, obj_frence))
			{
				var s = sign(hspd);
			
				while(!place_meeting(x + s, y, obj_frence))
					x += s;
	
				hspd = 0;
			}
			x += hspd;
		break;
	
		case S_ATTACK_1:
			scr_set_animation(s_skeleton_attack_one_strip)
	
			if(image_index == 0)
			{
				var hitbox = instance_create_layer(x, y, "Instances", obj_hitbox);
				hitbox.sprite_index = s_skeleton_attack_one_damage;
				hitbox.image_xscale = image_xscale;
				hitbox.damage = 1;
				hitbox.f_player = true;
				image_index++;
			}
			else if(keyboard_check_pressed(vk_space) && image_index >= 3)
				state = S_ATTACK_2;
		break;
	
		case S_ATTACK_2:
			scr_set_animation(s_skeleton_attack_two_strip)
		
			if(image_index == 1)
			{
				var hitbox = instance_create_layer(x, y, "Instances", obj_hitbox);
				hitbox.sprite_index = s_skeleton_attack_two_damage;
				hitbox.image_xscale = image_xscale;
				hitbox.damage = 2;
				hitbox.f_player = true;
				image_index++;
			}
			else if(keyboard_check_pressed(vk_space) && image_index >= 3)
				state = S_ATTACK_3;
		break;
	
		case S_ATTACK_3:
			scr_set_animation(s_skeleton_attack_three_strip);
		
			if(image_index == 2)
			{
				var hitbox = instance_create_layer(x, y, "Instances", obj_hitbox);
				hitbox.sprite_index = s_skeleton_attack_three_damage;
				hitbox.image_xscale = image_xscale;
				hitbox.damage = 4;
				hitbox.f_player = true;
				image_index++;
			}
		break;
	
		case S_HIT:
			var s = image_xscale * -2;
	
			scr_set_animation(s_skeleton_hitstun);
		
			if(!place_meeting(x + s, y, obj_frence))
				x += s;
		break;
	}

	layer_x(layer_get_id("Graves_1"), x * 0.05);
	layer_x(layer_get_id("Graves_2"), x * 0.20);
	layer_x(layer_get_id("Graves_3"), x * 0.35);
}
else if(state != S_DEAD)
{
	state = S_DEAD;
	sprite_index = noone;
	
	for(var i = 0; i < 10; i++)
	{
		var bone = instance_create_layer(x, y - 8, "Instances", obj_bone);
		bone.image_index = i;
		bone.image_xscale = image_xscale;
		bone.rot = 8;
		bone.spd = (2 + random(2)) * image_xscale * -1;
		bone.grav = (2 + irandom(4)) * -1;
	}
	
	with(obj_crow)
		f_hit = true;
	
	with(obj_knight)
		state = S_IDLE;
		
	with(obj_boss)
		state = S_IDLE;
}