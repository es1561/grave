/// @description 
draw_sprite(s_bar, 0, 16, 16);
draw_sprite_part(s_bar, 1, 0, 0, (hp / max_hp) * 400, 16, 16, 16);

draw_text(16, 32, "LVL: " + string(lvl));
draw_text(16, 48, "EXP: " + string(total_xp));
draw_text(92, 32, "Kill: " + string(kill));

draw_text(8, window_get_height() - 18, "ESC - Restart / C - ROll / SPACE - Attack");