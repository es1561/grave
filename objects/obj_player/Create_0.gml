/// @description

#macro S_MOVE 1
#macro S_ATTACK_1 2
#macro S_ATTACK_2 3
#macro S_ATTACK_3 4
#macro S_ROLL 5
#macro S_HIT 6
#macro S_DEAD 7
#macro S_IDLE 8

state = S_MOVE;

lvl = 1;
max_hp = 10;
hp = max_hp;
max_xp = 25;
xp = 0;

kill = 0;
total_xp = 0;

f_roll = false;
f_immune = false;