{
    "id": "1ec7baa8-2f31-4219-abd3-6567429aa256",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "094f2c1a-a7f1-4418-8145-1eccfa141552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "8b754141-33c9-4cc8-89f3-5daa3fb3d7b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "487cc73e-579b-4822-b1f0-e7ed9c7aca01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "aa4d2e20-2b09-4860-8890-3fe24355c71d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "9c5d5903-5583-4ec3-bff1-85c003df102b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "ba16030e-8f0e-4be6-92e6-bbcfe6cafc7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "626fd733-1706-458f-a5e8-7523fa5b3f46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "388c8309-0896-40b3-b81a-23551d692f74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        },
        {
            "id": "c830dd12-f362-474a-80f0-3b0ab29525a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "1ec7baa8-2f31-4219-abd3-6567429aa256"
        }
    ],
    "maskSpriteId": "ece147c0-6f04-4544-a414-b5a7d98f7a2f",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d17b850-2cfe-40ca-bbdc-39563a4a0b88",
    "visible": true
}