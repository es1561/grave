/// @description 
if(!f_hit && !(other.f_immune) && other.state != S_ROLL)
{
	f_hit = true;
	var my_damage = dmg;

	with(other)
	{
		f_immune = true;
		state = S_HIT;
		hp -= my_damage;
		alarm[0] = room_speed div 5;
	}
}