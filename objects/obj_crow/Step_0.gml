/// @description 
if(hp > 0)
{
	if(!f_hit)
	{
		mp_linear_step_object(obj_player.x, obj_player.y, spd, noone);
		image_xscale = x < obj_player.x ? 1 : -1;
	}
	else
	{
		x += image_xscale;
		y -= 0.5;
	}
}
else
	instance_destroy();