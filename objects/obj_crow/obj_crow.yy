{
    "id": "b9681fdc-d6c7-4747-b078-2239ac9c4676",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crow",
    "eventList": [
        {
            "id": "d01e0334-c7e8-459e-88b0-9e45ad1c8a12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b9681fdc-d6c7-4747-b078-2239ac9c4676"
        },
        {
            "id": "dcd41b53-cdce-439d-b93f-910430bf0356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9681fdc-d6c7-4747-b078-2239ac9c4676"
        },
        {
            "id": "b1549d5a-5609-4a65-badf-70bd9f8e4255",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1ec7baa8-2f31-4219-abd3-6567429aa256",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9681fdc-d6c7-4747-b078-2239ac9c4676"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "824a5592-2b2a-4e6f-86fb-bc7f549b9e3e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
    "visible": true
}