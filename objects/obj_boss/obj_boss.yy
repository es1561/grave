{
    "id": "1982fc92-f4b2-482b-8d6c-158ce3f5d2fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "f478a308-779b-412e-b33f-b808c0daff1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1982fc92-f4b2-482b-8d6c-158ce3f5d2fb"
        },
        {
            "id": "fedf0aa1-5043-42fc-a696-04a74de03d1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1982fc92-f4b2-482b-8d6c-158ce3f5d2fb"
        },
        {
            "id": "83d8b1c9-fc1d-4c6a-b7db-94374bff78ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1982fc92-f4b2-482b-8d6c-158ce3f5d2fb"
        },
        {
            "id": "1e44890f-bf59-4416-9c35-c82fab858be2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "1982fc92-f4b2-482b-8d6c-158ce3f5d2fb"
        },
        {
            "id": "ea2cf87b-9bd8-48d2-8a11-2b8f5c050ee5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1982fc92-f4b2-482b-8d6c-158ce3f5d2fb"
        }
    ],
    "maskSpriteId": "40a871b1-ae2e-422c-b2f4-7783165eb2fe",
    "overriddenProperties": null,
    "parentObjectId": "824a5592-2b2a-4e6f-86fb-bc7f549b9e3e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "82a46839-52c7-4add-92e4-427017f17649",
    "visible": true
}