/// @description 

if(obj_player.state != S_DEAD)
{
	var side = irandom(10) mod 2;
	var enemy = counter < 10 ? choose(obj_crow, obj_knight) : obj_boss;

	switch(side)
	{
		case 0://left
			instance_create_layer(64,608, "Instances", enemy);
		break;
	
		case 1://right
			instance_create_layer(1536,608, "Instances", enemy);
		break;
	}

	counter++;
	if(counter > 10)
		counter = 0;

	alarm[0] = room_speed * 2;
}

