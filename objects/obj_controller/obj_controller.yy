{
    "id": "54dceca9-a4dd-40e6-8874-36c0d0db2519",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller",
    "eventList": [
        {
            "id": "59e04f4e-02c1-4c34-a667-fee51b7d5ec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54dceca9-a4dd-40e6-8874-36c0d0db2519"
        },
        {
            "id": "a314115c-8a02-4b1b-8af7-0f97459fc609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "54dceca9-a4dd-40e6-8874-36c0d0db2519"
        },
        {
            "id": "fed12cb8-0f67-42db-80f9-0557c36f4cff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54dceca9-a4dd-40e6-8874-36c0d0db2519"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}