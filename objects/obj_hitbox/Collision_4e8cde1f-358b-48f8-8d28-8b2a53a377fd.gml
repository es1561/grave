/// @description 
if(f_player)
{
	if(!ds_map_exists(ds_hit, other.id))
	{
		var my_damage = damage;
	
		with(other)
		{
			state = S_HIT;
			hp -= my_damage;
			alarm[0] = room_speed div 3;
		}
	
		ds_hit[? other.id] = true;
	}
}