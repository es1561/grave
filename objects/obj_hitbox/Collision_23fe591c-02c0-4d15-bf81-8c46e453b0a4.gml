/// @description 
if(!f_player)
{
	if(!(other.f_immune) && other.state != S_ROLL && !ds_map_exists(ds_hit, other.id))
	{
		var my_damage = damage;
	
		with(other)
		{
			f_immune = true;
			state = S_HIT;
			hp -= my_damage;
			alarm[0] = room_speed div 5;
		}
	
		ds_hit[? other.id] = true;
	}
}