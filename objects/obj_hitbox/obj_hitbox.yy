{
    "id": "00010353-4849-45cc-9f57-21aa9b7ff9b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hitbox",
    "eventList": [
        {
            "id": "4e8cde1f-358b-48f8-8d28-8b2a53a377fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "824a5592-2b2a-4e6f-86fb-bc7f549b9e3e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "00010353-4849-45cc-9f57-21aa9b7ff9b2"
        },
        {
            "id": "da879559-d701-4b4f-8614-520469cde93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00010353-4849-45cc-9f57-21aa9b7ff9b2"
        },
        {
            "id": "91c3c1a5-c59e-429b-9661-0b48975a512b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "00010353-4849-45cc-9f57-21aa9b7ff9b2"
        },
        {
            "id": "4335793c-0d41-441b-af02-3dfb01757941",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "00010353-4849-45cc-9f57-21aa9b7ff9b2"
        },
        {
            "id": "23fe591c-02c0-4d15-bf81-8c46e453b0a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1ec7baa8-2f31-4219-abd3-6567429aa256",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "00010353-4849-45cc-9f57-21aa9b7ff9b2"
        },
        {
            "id": "a4c92a80-7806-4364-8d14-6163b94667ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "00010353-4849-45cc-9f57-21aa9b7ff9b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}