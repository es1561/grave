/// @description 
if(!f_out)
{
	with(obj_player)
		kill++;
	
	repeat(xp)
	{
		var orb = instance_create_layer(x, y, "Instances", obj_orb);
		orb.direction = irandom(20) * 9;
		orb.speed = 1;
	}
}