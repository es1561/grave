{
    "id": "cac6befe-a3ae-459e-9f04-1534fcee3cd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_three_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 36,
    "bbox_right": 90,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e90a1248-ab7b-4b47-8ad4-a204b137ca8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cac6befe-a3ae-459e-9f04-1534fcee3cd2",
            "compositeImage": {
                "id": "91eadd10-6994-409f-95dd-14ea6b703db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90a1248-ab7b-4b47-8ad4-a204b137ca8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7cc74d-1211-43a5-8426-7415f2b18e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90a1248-ab7b-4b47-8ad4-a204b137ca8c",
                    "LayerId": "893c492e-de7e-4586-8037-97a0e46e7027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "893c492e-de7e-4586-8037-97a0e46e7027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cac6befe-a3ae-459e-9f04-1534fcee3cd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 30,
    "yorig": 47
}