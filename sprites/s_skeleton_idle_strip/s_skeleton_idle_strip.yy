{
    "id": "6d17b850-2cfe-40ca-bbdc-39563a4a0b88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_idle_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e317be21-ff21-41a9-b95f-040011adc716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d17b850-2cfe-40ca-bbdc-39563a4a0b88",
            "compositeImage": {
                "id": "18c26606-7e00-4829-8f89-a34170617063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e317be21-ff21-41a9-b95f-040011adc716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904dd419-a5d1-4246-8770-5e0d49a50e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e317be21-ff21-41a9-b95f-040011adc716",
                    "LayerId": "515b0c1a-b7f4-4a1f-8704-0bbd0ca14d1c"
                }
            ]
        },
        {
            "id": "c725ca22-5a6e-4983-91d8-faaaaf18c1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d17b850-2cfe-40ca-bbdc-39563a4a0b88",
            "compositeImage": {
                "id": "d7efe0e9-18e4-45a6-8057-079a2c0bbb90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c725ca22-5a6e-4983-91d8-faaaaf18c1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb9b3ae-c661-4d6c-ac60-5f73e31453ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c725ca22-5a6e-4983-91d8-faaaaf18c1cf",
                    "LayerId": "515b0c1a-b7f4-4a1f-8704-0bbd0ca14d1c"
                }
            ]
        },
        {
            "id": "e1b94b7b-a0c7-4cca-a0da-591d08d243c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d17b850-2cfe-40ca-bbdc-39563a4a0b88",
            "compositeImage": {
                "id": "c811cca5-2275-494e-89d2-26a293b8dae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b94b7b-a0c7-4cca-a0da-591d08d243c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a96ca16-ff96-49ef-8ecb-f8559f188c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b94b7b-a0c7-4cca-a0da-591d08d243c0",
                    "LayerId": "515b0c1a-b7f4-4a1f-8704-0bbd0ca14d1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "515b0c1a-b7f4-4a1f-8704-0bbd0ca14d1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d17b850-2cfe-40ca-bbdc-39563a4a0b88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 47
}