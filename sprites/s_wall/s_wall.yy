{
    "id": "bcfc2d1d-b032-45f8-87c3-1f752630e7a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec3e4394-d0eb-4a20-9167-0f38080c5d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcfc2d1d-b032-45f8-87c3-1f752630e7a5",
            "compositeImage": {
                "id": "13173d65-760d-49f2-87c0-32db74bb0318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3e4394-d0eb-4a20-9167-0f38080c5d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5628a1c8-a478-41d5-a0bb-b598cfeb9f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3e4394-d0eb-4a20-9167-0f38080c5d61",
                    "LayerId": "ef8b915c-7cd1-4bdb-a24f-612ec667dc28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef8b915c-7cd1-4bdb-a24f-612ec667dc28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcfc2d1d-b032-45f8-87c3-1f752630e7a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}