{
    "id": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_bones_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6523119-3e4b-46ec-ab33-ed324775a194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "c70dc995-a462-4303-be44-5a57a661ce71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6523119-3e4b-46ec-ab33-ed324775a194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a54613c1-0622-4551-adcb-2247cb992551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6523119-3e4b-46ec-ab33-ed324775a194",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "b7b30a42-24cf-4d61-a1d1-66d8d88a21bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "3c26eaea-4fae-4f38-b169-53880f677c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7b30a42-24cf-4d61-a1d1-66d8d88a21bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580dd673-edf7-407b-9595-94eef9bace1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7b30a42-24cf-4d61-a1d1-66d8d88a21bd",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "33ab22be-1a6a-4cb0-a229-d8de92146e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "8f819d2e-c374-4bbb-a42f-e575ed0d4a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ab22be-1a6a-4cb0-a229-d8de92146e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "168f0400-a092-4340-9e7c-a9e3c97d2bda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ab22be-1a6a-4cb0-a229-d8de92146e8b",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "47979f4e-85c6-44ed-be61-048261e67d5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "a11174a6-da72-4a17-9d51-c2145b11950f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47979f4e-85c6-44ed-be61-048261e67d5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab778d12-8263-4491-ab64-e68cbf163568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47979f4e-85c6-44ed-be61-048261e67d5c",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "6dc54e3e-b33c-40c9-9f8d-ccc2cc822e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "d4436039-d84a-4e82-91b6-aef93b87714d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dc54e3e-b33c-40c9-9f8d-ccc2cc822e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e1036e-1c31-49b0-b7a9-5034e12eddcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dc54e3e-b33c-40c9-9f8d-ccc2cc822e7a",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "1d00cb01-811d-459b-b718-f82147354aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "833d002a-fd17-4888-ab42-b3e14dc0621a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d00cb01-811d-459b-b718-f82147354aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "229c42a6-fd60-43ec-a171-c7df6e23fbb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d00cb01-811d-459b-b718-f82147354aa2",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "bd0db090-5bef-4434-bc46-af7f353e15c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "d2d2556a-9cc3-4187-b93f-dd7e362b539e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0db090-5bef-4434-bc46-af7f353e15c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f827eb4b-40e7-4028-8bd1-bf9445a67ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0db090-5bef-4434-bc46-af7f353e15c5",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "279481f4-f387-46d4-ae00-53532f1c2e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "db582c5e-d970-46c7-93dc-e51ca5ea4313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "279481f4-f387-46d4-ae00-53532f1c2e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "713510cc-f28d-4bf1-b105-478a80d760e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "279481f4-f387-46d4-ae00-53532f1c2e5a",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "7364ee3f-f534-4b8b-b64f-974efc0ddbe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "d3b2586d-1ae9-4557-ab62-90a94d7df5bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7364ee3f-f534-4b8b-b64f-974efc0ddbe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ddf437-30c6-4b33-b291-69f9d572e965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7364ee3f-f534-4b8b-b64f-974efc0ddbe3",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        },
        {
            "id": "c1ac8fd3-a616-44f2-a55b-84298703f4cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "compositeImage": {
                "id": "13934d71-e741-4b0c-82c2-3a4a46b4b72b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ac8fd3-a616-44f2-a55b-84298703f4cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64e5d0f5-1269-46b2-b793-47a0a7f5859d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ac8fd3-a616-44f2-a55b-84298703f4cd",
                    "LayerId": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "0dd05a02-b227-4efe-ab2f-e6fdf93ebe62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35dd144d-8e49-484c-873b-9ee6ccd120ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 10
}