{
    "id": "23e148a5-ba7a-4cf2-98f6-959f412f9081",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 6,
    "bbox_right": 36,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8f5be18-2f34-47ea-92a7-b07fb04e68ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23e148a5-ba7a-4cf2-98f6-959f412f9081",
            "compositeImage": {
                "id": "920f9a0b-6d0b-4971-b06d-760662d89268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f5be18-2f34-47ea-92a7-b07fb04e68ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9870e73-2a67-4656-95b3-32b26731a7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f5be18-2f34-47ea-92a7-b07fb04e68ca",
                    "LayerId": "e1a937f2-9a2f-4a2f-b6ba-9ebdd10301c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e1a937f2-9a2f-4a2f-b6ba-9ebdd10301c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23e148a5-ba7a-4cf2-98f6-959f412f9081",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 24,
    "yorig": 47
}