{
    "id": "12e83484-86bb-4b60-8310-1e712355e4df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_attack_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 58,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e763d8fe-ad8e-48ea-bdda-e579a647f67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "1207307f-97be-41ab-8355-0ee4e4aed285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e763d8fe-ad8e-48ea-bdda-e579a647f67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d339f92-ec53-4aab-acca-4aa045f7b525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e763d8fe-ad8e-48ea-bdda-e579a647f67e",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "6b295340-8222-4434-b382-37312b4ab49a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "7a2b2e91-a524-4de7-a879-6793822c0430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b295340-8222-4434-b382-37312b4ab49a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "503aeeea-1219-4a90-88d6-3ddc9211f632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b295340-8222-4434-b382-37312b4ab49a",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "664ae839-0cb7-4a0c-8f17-319c84a638a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "2024a00d-3f1e-4b14-a9ce-d28450aa198e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "664ae839-0cb7-4a0c-8f17-319c84a638a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4cfc873-e14c-4d9a-aa99-a5f6e73f7543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "664ae839-0cb7-4a0c-8f17-319c84a638a4",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "fba29b27-26e0-4cbd-ba87-5147d02ee666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "77b1a4d2-5ffa-4f9d-a711-f098c2a3c030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba29b27-26e0-4cbd-ba87-5147d02ee666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdbf9304-bd78-49bf-b4c3-5adf1598c0cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba29b27-26e0-4cbd-ba87-5147d02ee666",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "82d24668-fbd0-409a-8bcf-269975faa728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "32faf9d0-9a48-42f0-abc3-0bff4bd0678c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d24668-fbd0-409a-8bcf-269975faa728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "089089d4-76bf-48c6-8dac-301a8de0b02a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d24668-fbd0-409a-8bcf-269975faa728",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "8c36d05b-384a-4470-8638-086a0ed50ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "c0f5d5d7-2cc6-49e4-a764-eb973b3127be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c36d05b-384a-4470-8638-086a0ed50ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef135212-d0eb-429a-933c-97552400fa12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c36d05b-384a-4470-8638-086a0ed50ae8",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "aa7432c4-d306-4d71-b936-5e842cb9efba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "156223b7-07ef-4c3a-a023-5a984dac2ec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7432c4-d306-4d71-b936-5e842cb9efba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1060cc9b-a035-4cb1-9959-5684d0ba248d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7432c4-d306-4d71-b936-5e842cb9efba",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "fffc7b99-eca1-4d10-9944-3b94110346d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "d4ca1335-aa27-4f1e-84ac-a2d436313816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fffc7b99-eca1-4d10-9944-3b94110346d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a288b36e-deaa-4d10-8b36-d118157456c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fffc7b99-eca1-4d10-9944-3b94110346d2",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "c53f257a-9b5c-4ae8-a228-b61cf0d3b771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "ebeaf1b0-d3eb-4b26-8457-e70f4cd87760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c53f257a-9b5c-4ae8-a228-b61cf0d3b771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdfe04d-462c-4678-bc19-85296990586a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c53f257a-9b5c-4ae8-a228-b61cf0d3b771",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "bca7981d-9c9e-4031-9119-1ef23c75ec05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "5e3c997d-fe32-4b2e-b461-84a87dc0bb53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca7981d-9c9e-4031-9119-1ef23c75ec05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3f412c-daa4-4e4f-b110-5879563fbd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca7981d-9c9e-4031-9119-1ef23c75ec05",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "d9b63a74-99b2-4976-948c-1c21d82d4093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "527e31b3-10be-4202-8ce1-5932763de152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9b63a74-99b2-4976-948c-1c21d82d4093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "280c3b4c-fa73-4815-9eff-41ee7590af2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b63a74-99b2-4976-948c-1c21d82d4093",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        },
        {
            "id": "408eb6a7-77f5-49b0-ac56-aaec1b1be9f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "compositeImage": {
                "id": "d2a64b8a-2b38-4cf2-9705-d7f8bd142b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "408eb6a7-77f5-49b0-ac56-aaec1b1be9f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83d1ddc-5561-4a73-af1a-4515d2138d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "408eb6a7-77f5-49b0-ac56-aaec1b1be9f2",
                    "LayerId": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "06f5e7ce-6be9-4f3b-93f9-17c1814813a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12e83484-86bb-4b60-8310-1e712355e4df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 20,
    "yorig": 47
}