{
    "id": "378e8daf-f11a-42fb-ac3d-e19a95502152",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "585584ac-c867-49e7-ad11-09be7126530e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "378e8daf-f11a-42fb-ac3d-e19a95502152",
            "compositeImage": {
                "id": "d7623e45-dbde-4d97-a4fe-ac37a760a226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585584ac-c867-49e7-ad11-09be7126530e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7be970d-6973-4339-ac5a-eac7f07741be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585584ac-c867-49e7-ad11-09be7126530e",
                    "LayerId": "2a1d3f17-5107-43d4-b42b-1047726c85f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2a1d3f17-5107-43d4-b42b-1047726c85f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "378e8daf-f11a-42fb-ac3d-e19a95502152",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 39
}