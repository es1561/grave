{
    "id": "7c1abce4-6d72-45df-959e-903c703c564f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dfea764-565c-4086-b610-176aced3997f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c1abce4-6d72-45df-959e-903c703c564f",
            "compositeImage": {
                "id": "da677859-e5aa-49cb-9729-a116a05c9d47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dfea764-565c-4086-b610-176aced3997f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a8c2556-37f3-4a33-8380-fbc925bb90cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dfea764-565c-4086-b610-176aced3997f",
                    "LayerId": "aaeb1e31-e130-4834-adec-6852eaacab85"
                }
            ]
        },
        {
            "id": "273de061-7c8c-451c-b836-57676d244198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c1abce4-6d72-45df-959e-903c703c564f",
            "compositeImage": {
                "id": "abdeb973-0e0e-4b18-b078-542de1d49a1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273de061-7c8c-451c-b836-57676d244198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0947f73a-8faf-40b8-b2d7-3fd555c5aa7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273de061-7c8c-451c-b836-57676d244198",
                    "LayerId": "aaeb1e31-e130-4834-adec-6852eaacab85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "aaeb1e31-e130-4834-adec-6852eaacab85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c1abce4-6d72-45df-959e-903c703c564f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}