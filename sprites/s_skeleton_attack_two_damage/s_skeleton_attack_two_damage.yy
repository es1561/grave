{
    "id": "f2c499ea-7357-4bf9-bfb3-21ef7b52c116",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_two_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 16,
    "bbox_right": 74,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82d4b960-4f88-4bac-b982-50566e955fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2c499ea-7357-4bf9-bfb3-21ef7b52c116",
            "compositeImage": {
                "id": "05717a19-b914-4e5d-857d-a22e7baeed87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d4b960-4f88-4bac-b982-50566e955fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7ed3a72-f665-4ef6-8c31-5ca25256e881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d4b960-4f88-4bac-b982-50566e955fd0",
                    "LayerId": "24a2c1a3-79c8-4d04-b0df-d1c2efe66d1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "24a2c1a3-79c8-4d04-b0df-d1c2efe66d1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2c499ea-7357-4bf9-bfb3-21ef7b52c116",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 29,
    "yorig": 47
}