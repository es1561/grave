{
    "id": "3d1ee3fd-3d1c-4944-bfc6-0f69bdfa2e06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grave_strip3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfb8e6c8-2d36-491f-a0d0-a1628122a360",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d1ee3fd-3d1c-4944-bfc6-0f69bdfa2e06",
            "compositeImage": {
                "id": "cebdd83d-75ad-4c3b-9006-ede0f4764d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb8e6c8-2d36-491f-a0d0-a1628122a360",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4353c058-95b9-4259-b602-ee8b3c13f45c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb8e6c8-2d36-491f-a0d0-a1628122a360",
                    "LayerId": "17dd432c-3c0d-40a1-ad45-0996d01e106f"
                }
            ]
        },
        {
            "id": "4bbeaaf0-7e55-454e-889a-78003fab3a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d1ee3fd-3d1c-4944-bfc6-0f69bdfa2e06",
            "compositeImage": {
                "id": "821fe4da-65c5-4ce1-be52-aa0ad971cade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bbeaaf0-7e55-454e-889a-78003fab3a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d5c93f-08e6-4833-99d9-24e97826aefb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bbeaaf0-7e55-454e-889a-78003fab3a05",
                    "LayerId": "17dd432c-3c0d-40a1-ad45-0996d01e106f"
                }
            ]
        },
        {
            "id": "bc5b8638-beab-4f2c-b96f-18fb7a682497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d1ee3fd-3d1c-4944-bfc6-0f69bdfa2e06",
            "compositeImage": {
                "id": "1d3589da-778c-44d4-8860-f6db3469220e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5b8638-beab-4f2c-b96f-18fb7a682497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9de2fc-fd09-42ea-b936-965616d003b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5b8638-beab-4f2c-b96f-18fb7a682497",
                    "LayerId": "17dd432c-3c0d-40a1-ad45-0996d01e106f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "17dd432c-3c0d-40a1-ad45-0996d01e106f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d1ee3fd-3d1c-4944-bfc6-0f69bdfa2e06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 39
}