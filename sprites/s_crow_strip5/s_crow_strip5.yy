{
    "id": "1945e376-e38b-4bff-88ea-c465d3022daa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_crow_strip5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3709802-afcd-40b9-8560-c9d2698c2641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
            "compositeImage": {
                "id": "8d4a71c6-c875-4db1-8601-6d31dcba0c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3709802-afcd-40b9-8560-c9d2698c2641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909ffc9a-fd22-4521-ab2f-6b909e8ea681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3709802-afcd-40b9-8560-c9d2698c2641",
                    "LayerId": "e5c3e337-24b3-44f4-b3b1-d914e9b7acb2"
                }
            ]
        },
        {
            "id": "38a543b3-af60-4677-a0f1-1528f819fd64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
            "compositeImage": {
                "id": "c6aae15e-326e-491f-8f32-be3d05743585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a543b3-af60-4677-a0f1-1528f819fd64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f9a6a0-6e80-4197-bd53-802212037f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a543b3-af60-4677-a0f1-1528f819fd64",
                    "LayerId": "e5c3e337-24b3-44f4-b3b1-d914e9b7acb2"
                }
            ]
        },
        {
            "id": "27922b14-b1a8-4423-b50d-a0442d7206af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
            "compositeImage": {
                "id": "8b872e0b-e599-42e6-98c6-eab8da27869c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27922b14-b1a8-4423-b50d-a0442d7206af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24a147c0-fba5-4a8b-9046-98b467445dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27922b14-b1a8-4423-b50d-a0442d7206af",
                    "LayerId": "e5c3e337-24b3-44f4-b3b1-d914e9b7acb2"
                }
            ]
        },
        {
            "id": "1c9d1a0f-58b1-4dbc-a203-657de8939690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
            "compositeImage": {
                "id": "122c2ea4-1b9d-4b83-a40a-c58f97489f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9d1a0f-58b1-4dbc-a203-657de8939690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cf0e4e-87cd-4c46-9276-da8e335c167e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9d1a0f-58b1-4dbc-a203-657de8939690",
                    "LayerId": "e5c3e337-24b3-44f4-b3b1-d914e9b7acb2"
                }
            ]
        },
        {
            "id": "f58e7a4d-735f-4443-89e7-5e9229ff3f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
            "compositeImage": {
                "id": "fb4b4453-3f7b-4225-9b28-15a90fdab073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f58e7a4d-735f-4443-89e7-5e9229ff3f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fcd08a0-7865-4065-8f7a-dcc4107bf544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f58e7a4d-735f-4443-89e7-5e9229ff3f6e",
                    "LayerId": "e5c3e337-24b3-44f4-b3b1-d914e9b7acb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e5c3e337-24b3-44f4-b3b1-d914e9b7acb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1945e376-e38b-4bff-88ea-c465d3022daa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 47
}