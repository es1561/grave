{
    "id": "f2ecda4c-cba1-4559-b7ae-5bec760306ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_orb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85684366-dd1b-4236-addd-8ea0c987382a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ecda4c-cba1-4559-b7ae-5bec760306ba",
            "compositeImage": {
                "id": "ac72f308-e1bc-43af-8351-55b58f1dd649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85684366-dd1b-4236-addd-8ea0c987382a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797fcb3a-b134-456d-b93c-bca2f99ec776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85684366-dd1b-4236-addd-8ea0c987382a",
                    "LayerId": "4637c764-ca52-43e2-97b4-be8c569c0f49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "4637c764-ca52-43e2-97b4-be8c569c0f49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2ecda4c-cba1-4559-b7ae-5bec760306ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}