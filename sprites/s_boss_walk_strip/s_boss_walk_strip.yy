{
    "id": "b7fa5546-f686-4d12-9098-3abb7de2a499",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss_walk_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 10,
    "bbox_right": 90,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34827b41-7889-4a76-b0c1-8e4d9e28e16b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "compositeImage": {
                "id": "43a1d93f-5acf-4344-90ac-ce0b024fb453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34827b41-7889-4a76-b0c1-8e4d9e28e16b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da5e8c95-3f67-4a91-94ce-33a2d7997820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34827b41-7889-4a76-b0c1-8e4d9e28e16b",
                    "LayerId": "e6ed0aea-3348-4330-be27-b966e9a37c3d"
                }
            ]
        },
        {
            "id": "cf902778-0f66-4c62-9402-2fac764d5027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "compositeImage": {
                "id": "c2d96e08-390b-4ae2-af38-6ccd0ce78d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf902778-0f66-4c62-9402-2fac764d5027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919a374e-7204-4765-81de-fb5dc8f8ae2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf902778-0f66-4c62-9402-2fac764d5027",
                    "LayerId": "e6ed0aea-3348-4330-be27-b966e9a37c3d"
                }
            ]
        },
        {
            "id": "39d58740-a5a3-418a-bc37-9b182c734871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "compositeImage": {
                "id": "98edaad8-03b8-4e1b-a6f1-c061452d4d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39d58740-a5a3-418a-bc37-9b182c734871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea9ac84-9d72-43df-aae6-0be519b46457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39d58740-a5a3-418a-bc37-9b182c734871",
                    "LayerId": "e6ed0aea-3348-4330-be27-b966e9a37c3d"
                }
            ]
        },
        {
            "id": "02a1bf57-0816-4fa9-ad95-8a39929d4597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "compositeImage": {
                "id": "8cdb85c0-5e8e-47e1-b8c7-0d5d2d8a9c7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02a1bf57-0816-4fa9-ad95-8a39929d4597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd79492-0f12-40a9-a201-99bd38e4a671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02a1bf57-0816-4fa9-ad95-8a39929d4597",
                    "LayerId": "e6ed0aea-3348-4330-be27-b966e9a37c3d"
                }
            ]
        },
        {
            "id": "bd0e3f57-e0f7-4081-8e2b-e7968d0dce08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "compositeImage": {
                "id": "e6df5898-c0ad-45c3-80d2-3d95c376a0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0e3f57-e0f7-4081-8e2b-e7968d0dce08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea416907-304c-418b-9b1b-c622d8b5021e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0e3f57-e0f7-4081-8e2b-e7968d0dce08",
                    "LayerId": "e6ed0aea-3348-4330-be27-b966e9a37c3d"
                }
            ]
        },
        {
            "id": "3f28848d-e08f-4c83-924c-0d5d1061d538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "compositeImage": {
                "id": "79c10958-1765-401f-a3e4-df2bcbe6cd2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f28848d-e08f-4c83-924c-0d5d1061d538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ef156ef-3a3e-4a56-8d31-67bc206b408c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f28848d-e08f-4c83-924c-0d5d1061d538",
                    "LayerId": "e6ed0aea-3348-4330-be27-b966e9a37c3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "e6ed0aea-3348-4330-be27-b966e9a37c3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7fa5546-f686-4d12-9098-3abb7de2a499",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 90
}