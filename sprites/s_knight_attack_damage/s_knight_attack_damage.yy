{
    "id": "2efb94fc-9a91-4272-9bbe-b8a027fda77f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_attack_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 5,
    "bbox_right": 57,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "825735a6-59bd-49b5-a366-43a99d16d93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2efb94fc-9a91-4272-9bbe-b8a027fda77f",
            "compositeImage": {
                "id": "a01f799b-b668-4677-aa1b-83af9159efff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825735a6-59bd-49b5-a366-43a99d16d93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145b7b72-e022-4655-b29c-760ad1e159cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825735a6-59bd-49b5-a366-43a99d16d93c",
                    "LayerId": "494006eb-5ba9-4ccd-8a51-9cc328d71d34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "494006eb-5ba9-4ccd-8a51-9cc328d71d34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2efb94fc-9a91-4272-9bbe-b8a027fda77f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 20,
    "yorig": 47
}