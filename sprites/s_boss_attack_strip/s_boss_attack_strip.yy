{
    "id": "74b20834-cfc9-44aa-af98-68a4c0543809",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss_attack_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 46,
    "bbox_right": 241,
    "bbox_top": 41,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b7cf83f-d6ce-47c2-874d-41a964037747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "ea2e7c10-08c6-453c-aef5-2ab3c275ad93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b7cf83f-d6ce-47c2-874d-41a964037747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e77c8d0-c413-4f24-9432-d7e7956d7fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b7cf83f-d6ce-47c2-874d-41a964037747",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "310e072e-cbcb-48ee-a5ad-d51fd8746471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "bfda39d6-6819-4032-add3-2b27ca8499fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310e072e-cbcb-48ee-a5ad-d51fd8746471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a849daa-9c61-422e-844f-1cfbc22e8a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310e072e-cbcb-48ee-a5ad-d51fd8746471",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "34030b41-c35a-4a67-baf6-c8c4d3604cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "e2e2c7da-5af0-4246-af30-9dfbc4ff53f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34030b41-c35a-4a67-baf6-c8c4d3604cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "972ba058-d941-466c-91f1-d6649a07d83d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34030b41-c35a-4a67-baf6-c8c4d3604cf2",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "17fee366-a1a4-47fa-8559-a7953ee41d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "e85d23a5-2014-466a-96a0-909d3da14a48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17fee366-a1a4-47fa-8559-a7953ee41d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff1328d-cee3-4387-a1d6-cfcbc9cd1930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17fee366-a1a4-47fa-8559-a7953ee41d85",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "46a91b5b-7ee7-4be7-8997-015d4647f75f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "f56ef503-78b4-4d9a-a16d-14e2969d9aa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a91b5b-7ee7-4be7-8997-015d4647f75f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e98a644-1a0d-4766-bc47-d3e7e88cffb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a91b5b-7ee7-4be7-8997-015d4647f75f",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "15b47368-814c-47ca-96fd-d85135ab47e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "022368aa-a0a1-4e12-9624-fee93b296e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b47368-814c-47ca-96fd-d85135ab47e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890c49db-0cc6-4c2e-89a3-61293a30bebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b47368-814c-47ca-96fd-d85135ab47e1",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "e112c755-b642-4121-b27a-b64e74a0a5f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "90ab5cd9-d0c5-43ab-8135-d438414195f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e112c755-b642-4121-b27a-b64e74a0a5f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "946903cd-65fe-4ea2-997d-480d392a7742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e112c755-b642-4121-b27a-b64e74a0a5f3",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "3f41cb82-0f89-4f1e-982b-c9f44e6138d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "446d0bfb-c1fa-4241-b830-5eec12606662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f41cb82-0f89-4f1e-982b-c9f44e6138d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7804b2fd-760f-463c-971f-e0b31c90347c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f41cb82-0f89-4f1e-982b-c9f44e6138d1",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "8e8952d9-5d66-4aa7-942d-121e28d680d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "adb1aa4e-df8e-441a-96db-5f27edaf1cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8952d9-5d66-4aa7-942d-121e28d680d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e275b0c1-7248-445d-9795-1f7703043bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8952d9-5d66-4aa7-942d-121e28d680d2",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "527bb81a-3671-43bf-8784-57a65e40a6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "e40d6270-4490-4422-b9d4-3e39351d9bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "527bb81a-3671-43bf-8784-57a65e40a6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f39e30-b158-4ba4-bc35-25c506068f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "527bb81a-3671-43bf-8784-57a65e40a6a0",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "0550be04-d2b0-40c5-bb6a-278d312e6bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "a90ea368-b87e-49a1-a37e-1929eaabdb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0550be04-d2b0-40c5-bb6a-278d312e6bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8483fdc0-6d47-440b-aea5-0bab2ba5761c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0550be04-d2b0-40c5-bb6a-278d312e6bf1",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "49c837e9-5552-43b9-8cd9-b72f615d6a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "dc96b632-afca-4537-a9ea-230b29294029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c837e9-5552-43b9-8cd9-b72f615d6a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07480a7d-306b-4e94-8d4a-49db341354ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c837e9-5552-43b9-8cd9-b72f615d6a18",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "bed55321-0601-4d2d-a7af-c8e6439fe1be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "7af19155-d220-492c-965f-ed0ab0407874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed55321-0601-4d2d-a7af-c8e6439fe1be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a021e827-e2cc-4501-b33d-4c6aa09f4d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed55321-0601-4d2d-a7af-c8e6439fe1be",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "123d6c02-3d95-4b67-bf06-118212fe5bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "07c33e40-59c8-45ee-82e6-771061af21ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123d6c02-3d95-4b67-bf06-118212fe5bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25770571-f9e5-4d88-9a6b-28bc8b123606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123d6c02-3d95-4b67-bf06-118212fe5bda",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        },
        {
            "id": "52c129d9-966e-4ce8-a9f9-3bdb639885a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "compositeImage": {
                "id": "9d81702c-b524-4da5-84d8-edda0478632a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c129d9-966e-4ce8-a9f9-3bdb639885a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e88bf95-07fc-48a7-a6bc-afaa14c1a309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c129d9-966e-4ce8-a9f9-3bdb639885a5",
                    "LayerId": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "dbc6d42d-28c0-4f44-a2e6-ec282ba4b144",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74b20834-cfc9-44aa-af98-68a4c0543809",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 91,
    "yorig": 174
}