{
    "id": "f5cb645d-5b73-4c6c-8361-3b383b9f5f3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7614110a-a00f-4764-a5e6-712137705938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5cb645d-5b73-4c6c-8361-3b383b9f5f3f",
            "compositeImage": {
                "id": "73439b72-d038-4925-a5be-408af0d4693d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7614110a-a00f-4764-a5e6-712137705938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5192e73-7d64-4d66-9396-f6180a43245a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7614110a-a00f-4764-a5e6-712137705938",
                    "LayerId": "705b0d36-c931-40fc-a3d0-3a07e9bc61b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "705b0d36-c931-40fc-a3d0-3a07e9bc61b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5cb645d-5b73-4c6c-8361-3b383b9f5f3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 38
}