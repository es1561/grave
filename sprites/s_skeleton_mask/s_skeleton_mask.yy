{
    "id": "ece147c0-6f04-4544-a414-b5a7d98f7a2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b0fda2a-11df-49f0-89ed-4af3d1229565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece147c0-6f04-4544-a414-b5a7d98f7a2f",
            "compositeImage": {
                "id": "acbfaad4-85dd-4597-b3d3-16d2251403c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b0fda2a-11df-49f0-89ed-4af3d1229565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded8a3de-4611-4fa1-93d7-e1323b238d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b0fda2a-11df-49f0-89ed-4af3d1229565",
                    "LayerId": "6f786e2e-918d-415e-9a13-755bf9009514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "6f786e2e-918d-415e-9a13-755bf9009514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ece147c0-6f04-4544-a414-b5a7d98f7a2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 38
}