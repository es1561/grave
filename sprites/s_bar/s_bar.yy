{
    "id": "a8023bbb-8346-414d-8f7c-7acabac1bb13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3eb8cc3-11fe-4520-ba59-78f2dab64c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8023bbb-8346-414d-8f7c-7acabac1bb13",
            "compositeImage": {
                "id": "42df7db1-7e3c-4cae-907b-f2c15118d989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3eb8cc3-11fe-4520-ba59-78f2dab64c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18b6c641-df01-4889-9fc1-42640f07b75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3eb8cc3-11fe-4520-ba59-78f2dab64c2e",
                    "LayerId": "e83f9d47-1c8e-4c2a-9d3b-e6cbef992070"
                }
            ]
        },
        {
            "id": "0c76060d-5469-41eb-a9e5-d2efc80320ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8023bbb-8346-414d-8f7c-7acabac1bb13",
            "compositeImage": {
                "id": "12b887f9-a796-47b8-bc2f-8d489bc5f6fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c76060d-5469-41eb-a9e5-d2efc80320ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224d347a-91a9-4d14-94d6-5da50142a1ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c76060d-5469-41eb-a9e5-d2efc80320ca",
                    "LayerId": "e83f9d47-1c8e-4c2a-9d3b-e6cbef992070"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e83f9d47-1c8e-4c2a-9d3b-e6cbef992070",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8023bbb-8346-414d-8f7c-7acabac1bb13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}