{
    "id": "0074ad8f-a0bb-4da9-9e6e-760385ea8966",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_idle_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 33,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8b6b9ea-b250-43ca-9614-f3ce657afd7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0074ad8f-a0bb-4da9-9e6e-760385ea8966",
            "compositeImage": {
                "id": "dcecdd64-f5de-478b-9473-81e8b869273c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b6b9ea-b250-43ca-9614-f3ce657afd7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd468251-4142-4b96-b386-53eda5faee79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b6b9ea-b250-43ca-9614-f3ce657afd7d",
                    "LayerId": "44707b39-3307-4442-943c-d70b6a000810"
                }
            ]
        },
        {
            "id": "fb880125-4f64-46db-9655-8b1049fe9808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0074ad8f-a0bb-4da9-9e6e-760385ea8966",
            "compositeImage": {
                "id": "bc9eef4a-a797-4462-a101-d3b6f3b8e3eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb880125-4f64-46db-9655-8b1049fe9808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d848482-42a2-44e3-b768-d96fdfd60bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb880125-4f64-46db-9655-8b1049fe9808",
                    "LayerId": "44707b39-3307-4442-943c-d70b6a000810"
                }
            ]
        },
        {
            "id": "f249c3fa-ed42-4689-b753-c539c63fd66a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0074ad8f-a0bb-4da9-9e6e-760385ea8966",
            "compositeImage": {
                "id": "b8a6d18d-87af-4987-8da2-d87f6988c5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f249c3fa-ed42-4689-b753-c539c63fd66a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2d076d-85c9-4c01-a238-90191bd3cd3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f249c3fa-ed42-4689-b753-c539c63fd66a",
                    "LayerId": "44707b39-3307-4442-943c-d70b6a000810"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "44707b39-3307-4442-943c-d70b6a000810",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0074ad8f-a0bb-4da9-9e6e-760385ea8966",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 47
}