{
    "id": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_roll_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 1,
    "bbox_right": 38,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e296a21-6a71-41cc-a7a6-9dfd24a26ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "5ebb0d5c-ef2a-4b8f-a690-bc2c0476ead4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e296a21-6a71-41cc-a7a6-9dfd24a26ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00f69d8a-804f-48df-ad62-5bfb51ec761e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e296a21-6a71-41cc-a7a6-9dfd24a26ced",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        },
        {
            "id": "e6969392-5607-4e27-a18a-a8780914e356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "2cd4a3e2-e2e0-4e89-ac34-f28d27f8eea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6969392-5607-4e27-a18a-a8780914e356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7938a7ba-e7dd-44b7-9611-9802edc0e5bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6969392-5607-4e27-a18a-a8780914e356",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        },
        {
            "id": "04caea80-87a7-454a-b5fd-236af38e44b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "1e6da1cf-1afd-468b-b8ce-1a9a46df5266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04caea80-87a7-454a-b5fd-236af38e44b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "384e8132-cb3b-41a2-8f56-5e23a27601ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04caea80-87a7-454a-b5fd-236af38e44b7",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        },
        {
            "id": "c0b15558-29ad-4974-b697-ead17ca437b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "7f3b0c4b-7952-4930-8bf5-83a9ad8abd2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0b15558-29ad-4974-b697-ead17ca437b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "636f6b8e-88fd-4055-8c79-b1d7ff97b5fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0b15558-29ad-4974-b697-ead17ca437b7",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        },
        {
            "id": "50b140af-25d7-4a93-81f9-1c1c96447dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "0acea23a-aa90-442e-b132-07dca2630be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b140af-25d7-4a93-81f9-1c1c96447dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b546d2eb-ae13-468d-b982-4ddd30fb7d9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b140af-25d7-4a93-81f9-1c1c96447dc9",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        },
        {
            "id": "18ab0ac4-0e1e-494c-ac71-1771d25c30ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "7396be80-b529-440d-9739-de10f48d7576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18ab0ac4-0e1e-494c-ac71-1771d25c30ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda9df71-fade-4b9b-999e-11a2ccf4ffb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18ab0ac4-0e1e-494c-ac71-1771d25c30ef",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        },
        {
            "id": "a662f71d-efbf-45fb-836f-34b5a0a47bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "compositeImage": {
                "id": "a71e453b-9bf4-498d-a945-6afcde95acab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a662f71d-efbf-45fb-836f-34b5a0a47bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1915d298-4ca1-450d-b0fa-a841959a6547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a662f71d-efbf-45fb-836f-34b5a0a47bd8",
                    "LayerId": "666ad816-7fa9-45d6-b112-04d6bc2be207"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "666ad816-7fa9-45d6-b112-04d6bc2be207",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daaa9a9e-ac5f-44c3-aca3-c15580bce315",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 24,
    "yorig": 47
}