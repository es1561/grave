{
    "id": "dc82e625-2879-42a6-a027-34eb2b0613e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_feather",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12e16fdf-fa62-4491-b2d0-e3c789f343df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc82e625-2879-42a6-a027-34eb2b0613e1",
            "compositeImage": {
                "id": "4af4916c-22f9-4533-8339-e04c9eeda59a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e16fdf-fa62-4491-b2d0-e3c789f343df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd5c497-9439-452a-b69f-6f3b585679e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e16fdf-fa62-4491-b2d0-e3c789f343df",
                    "LayerId": "9567879d-3883-42e3-9b91-0d1c8ae1ec0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "9567879d-3883-42e3-9b91-0d1c8ae1ec0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc82e625-2879-42a6-a027-34eb2b0613e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 4
}