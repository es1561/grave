{
    "id": "82a46839-52c7-4add-92e4-427017f17649",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 6,
    "bbox_right": 82,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8868dc2d-2497-4549-94de-6bfaf7bb0abb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82a46839-52c7-4add-92e4-427017f17649",
            "compositeImage": {
                "id": "91fef35b-9d24-45de-b0c7-69abec566844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8868dc2d-2497-4549-94de-6bfaf7bb0abb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1167ebd8-3710-415b-b490-0ea9aad1a26f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8868dc2d-2497-4549-94de-6bfaf7bb0abb",
                    "LayerId": "8dc7a70b-457a-4aa0-9d21-f040cc891dd8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "8dc7a70b-457a-4aa0-9d21-f040cc891dd8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82a46839-52c7-4add-92e4-427017f17649",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 45,
    "yorig": 87
}