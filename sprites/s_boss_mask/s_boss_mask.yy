{
    "id": "40a871b1-ae2e-422c-b2f4-7783165eb2fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e92f3a7-eecb-4169-950d-21605ed4c110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a871b1-ae2e-422c-b2f4-7783165eb2fe",
            "compositeImage": {
                "id": "1ee9feae-5d65-4789-991f-c0266874a451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e92f3a7-eecb-4169-950d-21605ed4c110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4352a20-2388-4b5c-8d09-d79b734197c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e92f3a7-eecb-4169-950d-21605ed4c110",
                    "LayerId": "e294f3a3-2850-4374-bc09-b1f8dcc20bd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "e294f3a3-2850-4374-bc09-b1f8dcc20bd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40a871b1-ae2e-422c-b2f4-7783165eb2fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 90
}