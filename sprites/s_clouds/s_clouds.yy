{
    "id": "4826eb83-8b80-4876-8892-066f63c17ff6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_clouds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 20,
    "bbox_right": 628,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e14c62b-f666-4e6e-88ba-809f254ea69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4826eb83-8b80-4876-8892-066f63c17ff6",
            "compositeImage": {
                "id": "80ec8410-b7bc-4aee-9dd0-820259b97f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e14c62b-f666-4e6e-88ba-809f254ea69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d29948b8-77dd-4465-b17c-4772fcdedf46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e14c62b-f666-4e6e-88ba-809f254ea69c",
                    "LayerId": "b29330e9-b795-499a-92ee-d6903aa9e8ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "b29330e9-b795-499a-92ee-d6903aa9e8ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4826eb83-8b80-4876-8892-066f63c17ff6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}