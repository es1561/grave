{
    "id": "89db9689-36ef-4471-8f66-be280a104e9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss_die_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 6,
    "bbox_right": 89,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04e92fe3-b0bd-4ba8-a4fd-ae63aafc7e90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89db9689-36ef-4471-8f66-be280a104e9c",
            "compositeImage": {
                "id": "80f72dc7-134f-4e02-aa11-fb32c53944ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04e92fe3-b0bd-4ba8-a4fd-ae63aafc7e90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fce626e3-beac-418a-91fc-f4534ebb2c1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04e92fe3-b0bd-4ba8-a4fd-ae63aafc7e90",
                    "LayerId": "57e673be-8a24-475f-ac2b-253c79278d59"
                }
            ]
        },
        {
            "id": "2273c8ae-92f1-4ec0-b913-412a37202775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89db9689-36ef-4471-8f66-be280a104e9c",
            "compositeImage": {
                "id": "e56f284c-7ddd-4ff2-ad67-b98aa0f582db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2273c8ae-92f1-4ec0-b913-412a37202775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1d1708-f79a-46ce-9eba-05f4b8321993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2273c8ae-92f1-4ec0-b913-412a37202775",
                    "LayerId": "57e673be-8a24-475f-ac2b-253c79278d59"
                }
            ]
        },
        {
            "id": "0eb63ca3-dcae-4222-a36e-2921e4ce09b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89db9689-36ef-4471-8f66-be280a104e9c",
            "compositeImage": {
                "id": "caf1ad31-8001-4ddc-81cf-e7948df80489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eb63ca3-dcae-4222-a36e-2921e4ce09b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7de7cc-a5c1-414d-aa09-d4519e7247af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eb63ca3-dcae-4222-a36e-2921e4ce09b4",
                    "LayerId": "57e673be-8a24-475f-ac2b-253c79278d59"
                }
            ]
        },
        {
            "id": "5d8c318d-4e89-4990-a8e2-f5b17468b62c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89db9689-36ef-4471-8f66-be280a104e9c",
            "compositeImage": {
                "id": "50c36259-718b-4bd6-992a-1babeb1874c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8c318d-4e89-4990-a8e2-f5b17468b62c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97e287a-6eb4-45ca-a3cf-451f195019f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8c318d-4e89-4990-a8e2-f5b17468b62c",
                    "LayerId": "57e673be-8a24-475f-ac2b-253c79278d59"
                }
            ]
        },
        {
            "id": "c3e4a842-d844-4272-958a-8d4bc98ce422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89db9689-36ef-4471-8f66-be280a104e9c",
            "compositeImage": {
                "id": "12bb6f6a-ba00-4262-b876-335130ec0c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e4a842-d844-4272-958a-8d4bc98ce422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc1b184-4ce1-4646-86ae-2954c4548fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e4a842-d844-4272-958a-8d4bc98ce422",
                    "LayerId": "57e673be-8a24-475f-ac2b-253c79278d59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "57e673be-8a24-475f-ac2b-253c79278d59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89db9689-36ef-4471-8f66-be280a104e9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 45,
    "yorig": 88
}