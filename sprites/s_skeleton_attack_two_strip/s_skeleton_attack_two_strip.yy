{
    "id": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_two_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 16,
    "bbox_right": 76,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1283b5f2-3c69-48a4-9235-352ce2777eca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
            "compositeImage": {
                "id": "9b9daa6b-fe49-44f9-9733-61068e60d7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1283b5f2-3c69-48a4-9235-352ce2777eca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0afe01-845f-46a2-b291-44dcf22edc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1283b5f2-3c69-48a4-9235-352ce2777eca",
                    "LayerId": "199f86b7-7033-4c12-99ac-93ba6964329d"
                }
            ]
        },
        {
            "id": "cf270ce5-7f85-472f-894d-82d94bb42e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
            "compositeImage": {
                "id": "7bf9870e-5d7a-4b22-a639-61e25429c8dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf270ce5-7f85-472f-894d-82d94bb42e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd2afca-6d96-441e-82b3-27896ca02fda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf270ce5-7f85-472f-894d-82d94bb42e1e",
                    "LayerId": "199f86b7-7033-4c12-99ac-93ba6964329d"
                }
            ]
        },
        {
            "id": "313046ff-6cd7-4716-a18c-3b3a21affefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
            "compositeImage": {
                "id": "2bc40da7-2d99-4864-99c8-1b695ab48875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313046ff-6cd7-4716-a18c-3b3a21affefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f97f1b6-7845-47ab-a69d-945fb1627e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313046ff-6cd7-4716-a18c-3b3a21affefc",
                    "LayerId": "199f86b7-7033-4c12-99ac-93ba6964329d"
                }
            ]
        },
        {
            "id": "55406591-a291-469d-b872-84e0d0d75ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
            "compositeImage": {
                "id": "b7b5587f-3a29-4403-8725-7ad15132023a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55406591-a291-469d-b872-84e0d0d75ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb7be6d-ca70-4801-96de-93ff00ea75c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55406591-a291-469d-b872-84e0d0d75ee3",
                    "LayerId": "199f86b7-7033-4c12-99ac-93ba6964329d"
                }
            ]
        },
        {
            "id": "e5c81c08-873a-4f41-a882-cca87ec540fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
            "compositeImage": {
                "id": "da99e17b-8d91-484c-8235-2267a75c354e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c81c08-873a-4f41-a882-cca87ec540fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e39878e4-fed0-46a1-bbdd-e55d484c763d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c81c08-873a-4f41-a882-cca87ec540fe",
                    "LayerId": "199f86b7-7033-4c12-99ac-93ba6964329d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "199f86b7-7033-4c12-99ac-93ba6964329d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17c43fd4-3638-461f-aa5a-3bf6e13ee129",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 29,
    "yorig": 47
}