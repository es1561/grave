{
    "id": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_run_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 5,
    "bbox_right": 32,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "139cb311-526f-40df-bd2f-72e90e991035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "compositeImage": {
                "id": "238fd429-79a9-4209-ac86-ba289669dd41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "139cb311-526f-40df-bd2f-72e90e991035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef97e25-e734-4ae1-bcb2-5ff21284f391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "139cb311-526f-40df-bd2f-72e90e991035",
                    "LayerId": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2"
                }
            ]
        },
        {
            "id": "9507682b-cd53-46e9-a53c-a5311cfd34b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "compositeImage": {
                "id": "098ec7a1-76fb-498d-8766-a2b6e25cc3a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9507682b-cd53-46e9-a53c-a5311cfd34b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fe68346-71c7-4c66-8e6d-dad36ec8a874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9507682b-cd53-46e9-a53c-a5311cfd34b5",
                    "LayerId": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2"
                }
            ]
        },
        {
            "id": "73589a59-04ab-4d0d-aa79-52d269ff5d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "compositeImage": {
                "id": "87c3614f-ceb5-4e59-8061-852c6a0f5677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73589a59-04ab-4d0d-aa79-52d269ff5d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aa49b18-c321-41f2-aeb7-6f02f6dc680b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73589a59-04ab-4d0d-aa79-52d269ff5d35",
                    "LayerId": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2"
                }
            ]
        },
        {
            "id": "f4fe6d9f-ccef-4d07-89cd-fae743f6c85c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "compositeImage": {
                "id": "98703bb6-b5a4-43ec-9023-2b8609530c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4fe6d9f-ccef-4d07-89cd-fae743f6c85c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6368745b-1f67-462f-88f7-5a42f77cc162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4fe6d9f-ccef-4d07-89cd-fae743f6c85c",
                    "LayerId": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2"
                }
            ]
        },
        {
            "id": "4eb632c7-a30b-4d71-9055-4b82c00d30d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "compositeImage": {
                "id": "60be084b-87d4-449f-908e-ee95791b512e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb632c7-a30b-4d71-9055-4b82c00d30d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed5acf46-7759-4053-8bd5-6dfebb70d375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb632c7-a30b-4d71-9055-4b82c00d30d8",
                    "LayerId": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2"
                }
            ]
        },
        {
            "id": "84d71465-08de-4a35-a5f1-a42e555de12e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "compositeImage": {
                "id": "fe664944-83cc-41fc-938c-607c1ccbdee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d71465-08de-4a35-a5f1-a42e555de12e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844ef91f-5e64-4bcc-bbfa-3d64a4a9a442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d71465-08de-4a35-a5f1-a42e555de12e",
                    "LayerId": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7c8c3ec4-da38-4642-aa27-6f67c3d67ce2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7005756-8fc3-4a25-8f38-af3fd707d7ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 22,
    "yorig": 47
}