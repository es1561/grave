{
    "id": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_walk_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 33,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beaa38df-b9ca-42bb-b695-bb2cdde580e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
            "compositeImage": {
                "id": "93f9c32c-b0c1-4040-b45d-9cd0ae51f28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beaa38df-b9ca-42bb-b695-bb2cdde580e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a8426ab-d39c-42b1-b46f-abbda145e154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beaa38df-b9ca-42bb-b695-bb2cdde580e2",
                    "LayerId": "b32075b1-386b-4304-98f4-188e4ee93bcb"
                }
            ]
        },
        {
            "id": "8e228069-152e-458e-aa10-1c5707e1d481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
            "compositeImage": {
                "id": "0fda6181-ad70-4c24-98e4-4d8af548a516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e228069-152e-458e-aa10-1c5707e1d481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8753752d-e5c4-4de8-bc72-4f8da77ee6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e228069-152e-458e-aa10-1c5707e1d481",
                    "LayerId": "b32075b1-386b-4304-98f4-188e4ee93bcb"
                }
            ]
        },
        {
            "id": "924b04f1-1484-44cc-99e2-e1088aa6e932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
            "compositeImage": {
                "id": "41572adb-48a4-4717-b2c6-444659864060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "924b04f1-1484-44cc-99e2-e1088aa6e932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3a93c8-b710-40f6-92bb-aba23ece3dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "924b04f1-1484-44cc-99e2-e1088aa6e932",
                    "LayerId": "b32075b1-386b-4304-98f4-188e4ee93bcb"
                }
            ]
        },
        {
            "id": "351d84f9-8f16-4dca-853e-58b09e3f9da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
            "compositeImage": {
                "id": "440657da-a60c-451c-bb7a-82bca4d19ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351d84f9-8f16-4dca-853e-58b09e3f9da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55affc3b-578f-4050-a4b5-65bdeab37d55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351d84f9-8f16-4dca-853e-58b09e3f9da4",
                    "LayerId": "b32075b1-386b-4304-98f4-188e4ee93bcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b32075b1-386b-4304-98f4-188e4ee93bcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f89dc76a-49b1-446d-99b6-efb99af09cfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 47
}