{
    "id": "c3f984d4-d1d6-425f-8259-a44b3ab25707",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss_attack_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 171,
    "bbox_left": 46,
    "bbox_right": 239,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a816a82e-a666-4980-98c0-d224e7614fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3f984d4-d1d6-425f-8259-a44b3ab25707",
            "compositeImage": {
                "id": "919427d2-7634-473d-a964-3a18e5a4e966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a816a82e-a666-4980-98c0-d224e7614fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4b441b-9030-4a7c-9ad6-d657aa665b39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a816a82e-a666-4980-98c0-d224e7614fbf",
                    "LayerId": "e48abf23-d4fd-4884-aa99-d5dee7c0858d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "e48abf23-d4fd-4884-aa99-d5dee7c0858d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3f984d4-d1d6-425f-8259-a44b3ab25707",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 91,
    "yorig": 174
}