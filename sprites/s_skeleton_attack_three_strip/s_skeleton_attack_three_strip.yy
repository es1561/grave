{
    "id": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_three_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 21,
    "bbox_right": 90,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cd59592-8ec6-42fc-83bc-65d2ad0bbf6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "compositeImage": {
                "id": "c92e985a-626a-4e2f-bab6-1ac961942db3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd59592-8ec6-42fc-83bc-65d2ad0bbf6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1044230e-37b6-432f-ae55-3e408945e02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd59592-8ec6-42fc-83bc-65d2ad0bbf6f",
                    "LayerId": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d"
                }
            ]
        },
        {
            "id": "25583957-2585-4188-96a3-5c1339457a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "compositeImage": {
                "id": "71152528-88eb-4d50-ada0-ee538da6dc19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25583957-2585-4188-96a3-5c1339457a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12712153-f3b9-4d08-b194-269baa81087d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25583957-2585-4188-96a3-5c1339457a79",
                    "LayerId": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d"
                }
            ]
        },
        {
            "id": "03b3873a-dc3d-461b-b0c0-5345e8662f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "compositeImage": {
                "id": "b3951da4-7d32-44c3-8088-c59fa847f83f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b3873a-dc3d-461b-b0c0-5345e8662f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "302d7431-8f47-43c2-838d-4de0bc276d3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b3873a-dc3d-461b-b0c0-5345e8662f62",
                    "LayerId": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d"
                }
            ]
        },
        {
            "id": "d2bab193-6d82-4f5d-8f67-def6e23dac7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "compositeImage": {
                "id": "c687d4e0-7d6d-4ff3-825b-e32cca9f1b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2bab193-6d82-4f5d-8f67-def6e23dac7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ef3a9d2-f797-4a0c-99fb-56e3faf1b290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2bab193-6d82-4f5d-8f67-def6e23dac7b",
                    "LayerId": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d"
                }
            ]
        },
        {
            "id": "8ecbe343-f5fc-43b7-a624-23c7f2c22b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "compositeImage": {
                "id": "3648d3ac-a0a8-4102-a9b8-d5c2704e3204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ecbe343-f5fc-43b7-a624-23c7f2c22b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3ae9e4-43ab-4358-8fe1-c01f5ecfae45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ecbe343-f5fc-43b7-a624-23c7f2c22b83",
                    "LayerId": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d"
                }
            ]
        },
        {
            "id": "37a9dd3b-e0f6-4b69-99c2-20ca62cbfca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "compositeImage": {
                "id": "5a28bea1-a846-4421-b0e5-638888b30e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a9dd3b-e0f6-4b69-99c2-20ca62cbfca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1885eded-e478-41ea-9f8b-bd3fe96a395a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a9dd3b-e0f6-4b69-99c2-20ca62cbfca5",
                    "LayerId": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "84dd7e0a-62ae-4629-a1b0-d33f41c9084d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6906f6e-aeca-4cf3-aa03-411f5eed5f90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 30,
    "yorig": 47
}