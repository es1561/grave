{
    "id": "90a31b63-e7b5-474e-813b-b56237f78e68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_one_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 15,
    "bbox_right": 63,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a9d705b-8021-4965-b0fa-e9b293413f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a31b63-e7b5-474e-813b-b56237f78e68",
            "compositeImage": {
                "id": "2b0112b4-06a2-4e7b-81c8-7be16e1f2595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a9d705b-8021-4965-b0fa-e9b293413f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a90a1de-a6f9-45ed-99f6-aee231bb35fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a9d705b-8021-4965-b0fa-e9b293413f29",
                    "LayerId": "db7af2ef-0e8a-4350-8f19-f5b9f7ce4dbb"
                }
            ]
        },
        {
            "id": "026cdb3b-3d25-4d03-b8c8-d04318f07f98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a31b63-e7b5-474e-813b-b56237f78e68",
            "compositeImage": {
                "id": "bf5818fd-f1b5-44f4-bf02-cdcdbc03a1d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026cdb3b-3d25-4d03-b8c8-d04318f07f98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21fed443-9f5e-4c39-aafd-c6b60a07951b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026cdb3b-3d25-4d03-b8c8-d04318f07f98",
                    "LayerId": "db7af2ef-0e8a-4350-8f19-f5b9f7ce4dbb"
                }
            ]
        },
        {
            "id": "0553c368-316f-4d0a-965d-92f30aa05544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a31b63-e7b5-474e-813b-b56237f78e68",
            "compositeImage": {
                "id": "45b8444b-ebd4-4930-8592-41c252e6f439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0553c368-316f-4d0a-965d-92f30aa05544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a065c8b9-a423-4361-ae86-7ef9967a0f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0553c368-316f-4d0a-965d-92f30aa05544",
                    "LayerId": "db7af2ef-0e8a-4350-8f19-f5b9f7ce4dbb"
                }
            ]
        },
        {
            "id": "d5ef8b4b-036e-45ef-94cd-273b76bdf8d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a31b63-e7b5-474e-813b-b56237f78e68",
            "compositeImage": {
                "id": "f2c2160e-547d-412e-b5e0-61bf86f9a232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5ef8b4b-036e-45ef-94cd-273b76bdf8d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ee093e-b357-4359-b748-c1bfaff3411d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5ef8b4b-036e-45ef-94cd-273b76bdf8d7",
                    "LayerId": "db7af2ef-0e8a-4350-8f19-f5b9f7ce4dbb"
                }
            ]
        },
        {
            "id": "2775328a-acf8-47bc-9631-a8cf20a09600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90a31b63-e7b5-474e-813b-b56237f78e68",
            "compositeImage": {
                "id": "c63a2ed2-8b6a-4b1f-a9e3-c8bc7f22a471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2775328a-acf8-47bc-9631-a8cf20a09600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae85d4b-3d51-4957-81ed-6b7dea8c6b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2775328a-acf8-47bc-9631-a8cf20a09600",
                    "LayerId": "db7af2ef-0e8a-4350-8f19-f5b9f7ce4dbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "db7af2ef-0e8a-4350-8f19-f5b9f7ce4dbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90a31b63-e7b5-474e-813b-b56237f78e68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 26,
    "yorig": 47
}