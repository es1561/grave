{
    "id": "c787290e-c0c1-424e-b12c-6878e89238a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_graves",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9bc7cb0-a07f-4645-bd22-e6105caa354b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c787290e-c0c1-424e-b12c-6878e89238a0",
            "compositeImage": {
                "id": "af80f134-6c74-4bec-b78c-0b32c2ae6fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9bc7cb0-a07f-4645-bd22-e6105caa354b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a987ec15-cc85-4fba-bd81-50055428f0d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9bc7cb0-a07f-4645-bd22-e6105caa354b",
                    "LayerId": "b5747a34-cc8a-424d-a1bf-a38bbb5a42e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "b5747a34-cc8a-424d-a1bf-a38bbb5a42e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c787290e-c0c1-424e-b12c-6878e89238a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}