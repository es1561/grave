{
    "id": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_die_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 36,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1daf4efd-19ce-4777-b372-dc02ee4c1b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "compositeImage": {
                "id": "c0679fe7-4016-4b5a-95bb-f8b669d97ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daf4efd-19ce-4777-b372-dc02ee4c1b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a2fe85-f08f-41bf-9a48-5d8d0c35e252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daf4efd-19ce-4777-b372-dc02ee4c1b46",
                    "LayerId": "2323b46d-a26c-48fc-a6aa-3d1d41114d60"
                }
            ]
        },
        {
            "id": "5a264dc2-c369-4803-b834-14879cb8ab41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "compositeImage": {
                "id": "48e33c1f-a578-42e0-94db-51aafb7f72c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a264dc2-c369-4803-b834-14879cb8ab41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0207bc-8c19-4439-9388-e9764f346639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a264dc2-c369-4803-b834-14879cb8ab41",
                    "LayerId": "2323b46d-a26c-48fc-a6aa-3d1d41114d60"
                }
            ]
        },
        {
            "id": "eba27e70-1d1c-47fe-93cf-90cb209da4fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "compositeImage": {
                "id": "6dbe4ee0-22ed-4c88-af9b-63d6d5815c89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba27e70-1d1c-47fe-93cf-90cb209da4fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b94c69f-a7cd-4b3d-9907-ffa12645894a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba27e70-1d1c-47fe-93cf-90cb209da4fd",
                    "LayerId": "2323b46d-a26c-48fc-a6aa-3d1d41114d60"
                }
            ]
        },
        {
            "id": "29483596-f0d5-40da-b58d-57b8d312aa82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "compositeImage": {
                "id": "ad77d0fe-9655-4f5a-bf9a-0a82d0f8adb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29483596-f0d5-40da-b58d-57b8d312aa82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f7eb5ff-de39-4a31-bc87-0eb9e70f4d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29483596-f0d5-40da-b58d-57b8d312aa82",
                    "LayerId": "2323b46d-a26c-48fc-a6aa-3d1d41114d60"
                }
            ]
        },
        {
            "id": "25f81563-50d7-43d0-bdb5-beda367e776b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "compositeImage": {
                "id": "999941bd-689a-4ab4-a3c2-e84c8dde7981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f81563-50d7-43d0-bdb5-beda367e776b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66561561-e788-478f-af30-3f276e6db250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f81563-50d7-43d0-bdb5-beda367e776b",
                    "LayerId": "2323b46d-a26c-48fc-a6aa-3d1d41114d60"
                }
            ]
        },
        {
            "id": "6ef9ae79-916d-4247-a9bc-c76290b54e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "compositeImage": {
                "id": "374a7c80-d6e6-48a4-87f7-838ccb002ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef9ae79-916d-4247-a9bc-c76290b54e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04220ebf-e509-4578-8706-d453090766e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef9ae79-916d-4247-a9bc-c76290b54e65",
                    "LayerId": "2323b46d-a26c-48fc-a6aa-3d1d41114d60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2323b46d-a26c-48fc-a6aa-3d1d41114d60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3af3a23b-0abc-4c33-a7fd-767cb81a2e1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 20,
    "yorig": 47
}