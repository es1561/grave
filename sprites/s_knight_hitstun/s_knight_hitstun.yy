{
    "id": "a27e5c9f-3b28-4c64-9a1a-11e48df97d33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_knight_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 2,
    "bbox_right": 32,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4bd8b4c-955a-47d8-b99e-1081ef02377e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a27e5c9f-3b28-4c64-9a1a-11e48df97d33",
            "compositeImage": {
                "id": "21b8466b-ba6b-4b9b-86c8-b26a805eb57b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bd8b4c-955a-47d8-b99e-1081ef02377e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c426fc-d36f-4c7b-af42-558c7c5f5650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bd8b4c-955a-47d8-b99e-1081ef02377e",
                    "LayerId": "7d231425-a8d5-4137-860d-38a83e0173ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7d231425-a8d5-4137-860d-38a83e0173ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a27e5c9f-3b28-4c64-9a1a-11e48df97d33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 47
}