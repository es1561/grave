{
    "id": "2e161c78-8e2d-4775-b479-f3258c6613fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_skeleton_attack_one_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 11,
    "bbox_right": 63,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d557cae-2657-4177-a3e2-7f79907af2e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e161c78-8e2d-4775-b479-f3258c6613fc",
            "compositeImage": {
                "id": "1b997ec4-86df-4346-8198-f86f03cadc86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d557cae-2657-4177-a3e2-7f79907af2e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c659130-875d-4398-966a-4b75580a6732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d557cae-2657-4177-a3e2-7f79907af2e6",
                    "LayerId": "a282e6ab-e74c-4545-afe9-523bb23ace32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a282e6ab-e74c-4545-afe9-523bb23ace32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e161c78-8e2d-4775-b479-f3258c6613fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 26,
    "yorig": 47
}